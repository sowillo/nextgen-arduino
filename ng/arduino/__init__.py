__author__ = 'fAX'
import threading
import sys
import serial

from PyQt5.QtCore import pyqtSignal, QObject
try:
    from serial.tools.list_ports import comports,grep
except ImportError:
    comports = None
    
import logging
logger = logging.getLogger(__name__)

LF = serial.to_bytes([10])
CR = serial.to_bytes([13])
CRLF = serial.to_bytes([13, 10])

CONVERT_CRLF = 2
CONVERT_CR   = 1
CONVERT_LF   = 0
NEWLINE_CONVERISON_MAP = (LF, CR, CRLF)
LF_MODES = ('LF', 'CR', 'CR/LF')

REPR_MODES = ('raw', 'some control', 'all control', 'hex')

if sys.version_info >= (3, 0):
    def character(b):
        return b.decode('latin1')
else:
    def character(b):
        return b

def onoff( on ):
    return 'on' if on else 'off'

class Arduino(QObject):
    dataReady = pyqtSignal(str)

    def __init__(self, port=None, baudRate = 115200 ):
        super(Arduino, self).__init__()
        self.validPorts = self.find_arduinos()
        if not self.validPorts:
            raise FileNotFoundError("No Arduino found")

        self.repr_mode = 0

        # TODO Exit gracefully
        self.serial = serial.Serial(port if port else self.validPorts[0][0], baudRate )
        self.start()

    def find_arduinos(self):
        if not comports:
            raise "No com ports"

        sys.stderr.write('\n--- Available ports:\n')
        arduinos = list( grep('Arduino|USB Serial Port|2560|ttyACM\d+') )
        for port, desc, hwid in arduinos:
            sys.stderr.write('--- %-20s %s\n' % (port, desc))

        return arduinos

    def _start_reader(self):
        """Start _reader thread"""
        self._reader_alive = True
        # start serial->console thread
        self.receiver_thread = threading.Thread(target=self._reader)
        self.receiver_thread.setDaemon(True)
        self.receiver_thread.start()

    def _stop_reader(self):
        """Stop _reader thread only, wait for clean exit of thread"""
        self._reader_alive = False
        self.receiver_thread.join()

    def start(self):
        self.alive = True
        self._start_reader()

    def stop(self):
        self.alive = False

    def join(self):
        self.receiver_thread.join()

    def dump_port_settings(self):
        sys.stderr.write("\n--- Settings: %s  %s,%s,%s,%s\n" % (
                self.serial.portstr,
                self.serial.baudrate,
                self.serial.bytesize,
                self.serial.parity,
                self.serial.stopbits))
        sys.stderr.write('--- RTS: %-8s  DTR: %-8s  BREAK: %-8s\n' % (
                (self.rts_state and 'active' or 'inactive'),
                (self.dtr_state and 'active' or 'inactive'),
                (self.break_state and 'active' or 'inactive')))
        try:
            sys.stderr.write('--- CTS: %-8s  DSR: %-8s  RI: %-8s  CD: %-8s\n' % (
                    (self.serial.getCTS() and 'active' or 'inactive'),
                    (self.serial.getDSR() and 'active' or 'inactive'),
                    (self.serial.getRI() and 'active' or 'inactive'),
                    (self.serial.getCD() and 'active' or 'inactive')))
        except serial.SerialException:
            # on RFC 2217 ports it can happen to no modem state notification was
            # yet received. ignore this error.
            pass
        sys.stderr.write('--- software flow control: %s\n' % (self.serial.xonxoff and 'active' or 'inactive'))
        sys.stderr.write('--- hardware flow control: %s\n' % (self.serial.rtscts and 'active' or 'inactive'))
        sys.stderr.write('--- data escaping: %s  linefeed: %s\n' % (
                REPR_MODES[self.repr_mode],
                LF_MODES[self.convert_outgoing]))

    def _reader(self):
        """loop and copy serial->console"""
        line = ""
        try:
            while self.alive and self._reader_alive:
                data = character(self.serial.read(1))

                if data == '\r':
                    #print "Got data: {}".format( line )
                    self.dataReady.emit( line )
                    logger.debug( line )
                    line = ""
                elif data != '\n':
                    line = line + data

        except serial.SerialException:
            self.alive = False
            # would be nice if the console _reader could be interrupted at this
            # point...
            raise