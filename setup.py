#!/usr/bin/env python

from distutils.core import setup

setup(name='nextgen-arduino',
      version='1.0',
      description='Arduino Interface Module',
      author='Pavel Gurevich',
      author_email='pavl@sowillo.com',
      url='',
      packages=['ng.arduino'],
     )